// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// const userRoute = require("./routes/userRoute");

// Server
const app = express();


// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// app.use("/users", userRoute);
// app.use("/products", productRoute);


// Database Connection
mongoose.connect("mongodb+srv://sepienkhel19:admin123@zuitt-bootcamp.bnuv3za.mongodb.net/eCommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log(`Now connected to cloud database`));





// Server listening
app.listen(process.env.PORT || 5000, () => console.log(`Now connected to port ${process.env.PORT || 5000}`));